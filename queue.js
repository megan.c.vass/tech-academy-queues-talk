class Queue {
  constructor( capacity ) {
    this.capacity = capacity || Infinity;
    this._head = 0;
    this._tail = 0;
    this.storage = {};
  }

  enqueue( value ) {
    if (this.count() < this.capacity) {
      this.storage[ this._tail++ ] = value;
      return this.count();
    }
    console.log('Max capacity reached, please remove a value before enqueuing');
    return false;
  }

  dequeue() {
    if (this.count() === 0) {
      console.log('Nothing in the queue');
      return false;
    } else {
      let element = this.storage[this._head];
      delete this.storage[this._head];
      if (this._head < this._tail) {
        this._head++;
      }
      return element;
    }
  }

  peek() {
    return this.storage[this._head];
  }

  count() {
    return this._tail - this._head;
  }
}
module.exports = Queue;
