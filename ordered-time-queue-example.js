const Queue = require('./queue');
const Job = require('./job');

function getRandomJobTimeLength() {
  const min = 1000;
  const max = 5000;
  return Math.random() * ( max - min ) + min;
}

const myQueue = new Queue(4);

for( let i = 0; i <= 5; i++ ) {
  myQueue.enqueue(
    new Job(
      getRandomJobTimeLength()
    ).start()
  );
}

const checkQ = setInterval(()=>{
  if( myQueue.count() === 0 ) {
    clearInterval(checkQ);
    console.log('All jobs have been dequeued in order!');
    return;
  }
  if( myQueue.peek().isDone() ) {
    const jobQueueId = myQueue._head;
    const finishedPeekJob = myQueue.dequeue();
    console.log( `Job ${jobQueueId} finished; Time taken: ${finishedPeekJob.timeToDo}` );
  }
  return;
}, 1000);
