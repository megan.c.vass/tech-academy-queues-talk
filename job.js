class Job {
  constructor( timeToDo ) {
    this.timeToDo = timeToDo;
    this.done = false;
    this.timeout;
  }

  start() {
    this.timeout = setTimeout(() => {
      this.done = true;
    }, this.timeToDo || 1000 );
    return this;
  }

  stop() {
    clearTimeout( this.timeout );
    this.done = true;
    return this;
  }

  isDone() {
    return this.done;
  }
}
module.exports = Job;
